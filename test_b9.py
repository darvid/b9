import pendulum  # type: ignore
import pytest  # type: ignore

import b9


@pytest.fixture
def config():
    with open('examples/b9.toml') as f:
        config = b9.Config.loads(f.read())
    return config


@pytest.fixture
def watcher(config):
    return b9.Watcher(config=config)


def test_alerts(watcher):
    watch_loop = watcher.run()

    for i in range(2):
        alert = next(watch_loop)
        assert alert.rule.name == ['red_high', 'red_low'][i]
        assert alert.payloads[0]['sat_id'] == '1000'
        assert len(alert.payloads) == 3
        dt_start = pendulum.parse(alert.payloads[0]['ts'])
        dt_end = pendulum.parse(alert.payloads[-1]['ts'])
        assert (dt_end - dt_start).seconds < 300
