# b9

![Danger, Will Robinson](danger.gif)

[b9][1] provides a simple but easily extendable API and CLI for
consuming and alerting on ✨ *streaming* ✨ telemetry.

[1]: https://en.wikipedia.org/wiki/Robot_(Lost_in_Space)

## Installation

### From source

**b9** is not yet available on [PyPI](https://pypi.org/), so for now
installation from source is the recommended method of installation.

```bash
$ git clone https://gitlab.com/darvid/b9.git
```

[Poetry](https://poetry.eustace.io/) is recommended, but not required to
install **b9** and its dependencies. If you're not using Poetry, it is
highly recommended that you create a new
[virtualenv](https://docs.python-guide.org/dev/virtualenvs/) to avoid
polluting your system packages.

```shell
# With Poetry
$ poetry install .
$ poetry run b9d --version
b9d, version 0.1.0

# With pip
$ pip install -r requirements.txt
$ pip install -e .
```

## Usage

![](usage.gif)

### API usage

🚧 Sphinx documentation coming soon. 🚧

### CLI usage (b9d)

**b9** ships with a daemon, ``b9d``, that reads inputs and rules from a
given configuration file and outputs alerts to stdout as they are
triggered. (Additional output destinations will be added shortly.)

```bash
$ b9d --help
Usage: b9d [OPTIONS]

  Watch and alert on configurable telemetry events.

Options:
  -c, --config PATH  b9 config file.  [required]
  -v / --verbose     Enable verbose logging.
  --version          Show the version and exit.
  --help             Show this message and exit.
```

By default, only brief alert information is shown, but a complete JSON
formatted alert object (including all payloads that triggered the alert)
can be provided with the `--verbose` flag.

### Configuration

Detailed documentation to follow, but provided below is an example
TOML-formatted **b9** configuration file:

```toml
# **inputs** is a mapping of input names to input configuration
[inputs.telemetry]
path = "examples/telemetry.psv"
# format is actually not needed as long as the extension is guessable
format = "CSV"
# format_options are arbitrary keyword arguments that are passed to
# the given parser; in this case, `csv.DictReader`.
format_options = { delimiter = '|' }

# **rules** is a mapping of rule names to rule configuration
[rules.red_low]
timestamp_field = "ts"
id_field = "sat_id"
# the *evaluator* expression is evaluated with simpleeval, and has
# access to all variables from telemetry payloads, and Python primitives
evaluator = "float(raw_value) < float(red_low_lim)"
threshold = [3, 300]  # 3 events within 300 seconds

[rules.red_high]
timestamp_field = "ts"
id_field = "sat_id"
# the *evaluator* expression is evaluated with simpleeval, and has
# access to all variables from telemetry payloads, and Python primitives
evaluator = "float(raw_value) > float(red_high_lim)"
threshold = [3, 300]  # 3 events within 300 seconds
```
