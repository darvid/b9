"""b9: simple threshold-based alerting."""
import abc
import collections
import csv
import enum
import dataclasses
import json
import logging
import os
import sys
import typing
import uuid

import boltons.cacheutils  # type: ignore
import click
import click_completion  # type: ignore
import pendulum  # type: ignore
import pkg_resources
import simpleeval  # type: ignore
import structlog  # type: ignore
import structlog_pretty  # type: ignore
import toml


__version__ = pkg_resources.get_distribution('b9').version

logger = structlog.get_logger(__name__)
PayloadType = typing.Dict[str, typing.Any]
InputPayloadTuple = typing.Tuple['Input', PayloadType]
StreamType = typing.Generator[typing.Optional[str], str, None]


@enum.unique
class InputFormat(enum.Enum):
    CSV = enum.auto()
    # TODO: JSON and plaintext support

    def get_parser(self) -> typing.Type['FilesystemParser']:
        """Get the associated :class:`Parser` for the given format."""
        for parser in FilesystemParser.__subclasses__():
            if parser.__name__ == f'{self.name}Parser':
                return parser
        raise NotImplementedError(
            f'Parser for {self.name} format not implemented'
        )

    @classmethod
    def guess(cls, filename: str) -> 'InputFormat':
        """Guess supported :class:`InputFormat` from given filename."""
        ext: str = os.path.splitext(filename)[1]
        ext = ext.upper()[1:]
        try:
            return cls[ext]
        except NameError:
            # Handle popular extensions representing the same format
            if ext in ('TSV', 'PSV'):
                return cls.CSV
            raise


@dataclasses.dataclass
class Input:
    """Input configuration for telemetry or logging sources."""

    name: str
    path: str
    format_options: typing.Dict[str, typing.Union[bool, float, int, str]]
    format: dataclasses.InitVar[InputFormat] = dataclasses.field()
    parser: 'FilesystemParser' = dataclasses.field(init=False)

    # **state** is a simple 2d mapping of time bucket -> ID -> count.
    # Time buckets are UNIX timestamps divided by threshold seconds.
    # An LRU cache is sufficient if we can assume linear timestamps,
    # but this is obviously not the most robust or high performant data
    # structure for maintaining alert counter state.
    state: boltons.cacheutils.LRU = dataclasses.field(init=False)

    def __post_init__(
        self, format: typing.Optional[typing.Union[str, InputFormat]] = None
    ) -> None:
        if format is None:
            parser = InputFormat.guess(self.path).get_parser()
        elif isinstance(format, str):
            parser = InputFormat[format].get_parser()
        else:
            parser = format.get_parser()
        self.parser = parser(input=self)
        self.state = boltons.cacheutils.LRU(max_size=256)
        logger.debug('Created input', format=format, parser=parser.__name__)


@dataclasses.dataclass(frozen=True)
class Rule:
    """Configuration for threshold-based alerts."""

    name: str
    evaluator: str
    id_field: str
    threshold: typing.Tuple[int, int]
    timestamp_field: str
    timestamp_fmt: typing.Optional[str] = None
    timestamp_tz: typing.Optional[str] = 'UTC'

    def evaluate(
        self, input: Input, payload: PayloadType
    ) -> typing.Optional['Alert']:
        """Evaluates a rule and returns an :class:`Alert` if triggered.

        Args:
            input (:class:`Input`): An input configuration.
            payload (:obj:`dict`): The telemetry payload or other
                structured event serialized to a dictionary.

        Returns:
            :class:`Alert`: An alert instance if the rule passed.

        """
        ts_str = payload[self.timestamp_field]
        if self.timestamp_fmt is not None:
            ts = pendulum.from_format(
                ts_str, self.timestamp_fmt, tz=self.timestamp_tz
            )
        else:
            ts = pendulum.parse(ts_str, tz=self.timestamp_tz)
        triggered = simpleeval.simple_eval(self.evaluator, names=payload)
        if triggered:
            bucket = ts.int_timestamp // self.threshold[1]
            if bucket not in input.state:
                input.state[bucket] = collections.defaultdict(list)
            identifier = ':'.join([self.name, payload[self.id_field]])
            input.state[bucket][identifier].append(payload)
            if len(input.state[bucket][identifier]) >= self.threshold[0]:
                payloads = input.state[bucket].pop(identifier)
                return Alert(input, self, payloads)
        return None


@dataclasses.dataclass(frozen=True)
class Alert:
    """Represents a triggered alert."""

    input: 'Input'
    rule: 'Rule'
    payloads: typing.List[PayloadType]
    timestamp: pendulum.DateTime = dataclasses.field(
        default_factory=pendulum.now
    )
    uid: uuid.UUID = dataclasses.field(default_factory=uuid.uuid4)

    def asdict(self) -> PayloadType:
        """Build a JSON serializable alert object."""
        return {
            'payloads': self.payloads,
            'rule': self.rule.name,
            'timestamp': str(self.timestamp),
            'uuid': self.uid.hex,
        }


class AbstractParser(abc.ABC):
    """A simple streaming parser interface."""

    def __init__(self, input: Input):
        self.input = input
        self.initialize()

    @abc.abstractmethod
    def initialize(self) -> None:
        """Initialize the parser, including any file descriptors."""

    @abc.abstractmethod
    def parse_next(self) -> typing.Optional[PayloadType]:
        """Deserialize the next payload from a streaming reader."""


class FilesystemParser(AbstractParser):
    """A simple streaming flat-file parser."""

    def initialize(self) -> None:
        """Open a file descriptor and initialize the reader."""
        self.fp = open(self.input.path)

    def readline(self) -> str:
        """Read a line, returning an empty string at EOF."""
        line = self.fp.readline()
        if line:
            if line is not None:
                return line
        return ''


class CSVParser(FilesystemParser):
    """Parses a CSV or CSV-compatible format."""

    def initialize(self) -> None:
        """Initialize a streaming :class:`csv.DictReader`."""
        super().initialize()
        self.reader = csv.DictReader(  # type: ignore
            iter(self.readline, None),  # type: ignore
            **self.input.format_options,
        )

    def parse_next(self) -> typing.Optional[PayloadType]:
        """Deserialize the next line."""
        return next(self.reader)


@dataclasses.dataclass
class Config:
    """Reads and tracks b9 input and rule configuration."""

    inputs: typing.List[Input] = dataclasses.field(default_factory=list)
    rules: typing.List[Rule] = dataclasses.field(default_factory=list)

    @classmethod
    def loads(cls, s: str) -> 'Config':
        """Load configuration from a given TOML formatted string."""
        config = cls()
        config_obj = toml.loads(s)
        config.inputs = [
            Input(name=input_name, **input_options)
            for input_name, input_options in config_obj['inputs'].items()
        ]
        config.rules = [
            Rule(name=rule_name, **rule_options)
            for rule_name, rule_options in config_obj['rules'].items()
        ]
        logger.debug(
            'Configuration loaded',
            num_inputs=len(config.inputs),
            num_rules=len(config.rules),
        )
        return config


@dataclasses.dataclass(frozen=True)
class Watcher:
    """Follows all parser streams for events and generates alerts."""

    config: Config

    def follow_all(self) -> typing.Iterator[InputPayloadTuple]:
        """Tails all inputs for data."""
        while True:
            # TODO: Wait if no data received by any input
            for input in self.config.inputs:
                payload = input.parser.parse_next()
                if payload is not None:
                    yield (input, payload)

    def run(self) -> typing.Iterator[Alert]:
        """Watches for events and triggers alerts."""
        for ln, (input, payload) in enumerate(self.follow_all()):
            for rule in self.config.rules:
                alert = rule.evaluate(input, payload)
                if alert is not None:
                    yield alert


def setup_logging(verbose: bool = False) -> None:
    """Configure standard library and :mod:`structlog` logging."""
    log_processors = [
        structlog.stdlib.add_logger_name,
        structlog.stdlib.add_log_level,
        structlog.stdlib.PositionalArgumentsFormatter(),
        structlog.processors.TimeStamper(fmt="%Y-%m-%d %H:%M.%S"),
        structlog.processors.StackInfoRenderer(),
        structlog.processors.format_exc_info,
        structlog_pretty.SyntaxHighlighter(
            {'alert': 'json', 'payload': 'json'}
        ),
        structlog.dev.ConsoleRenderer(),
    ]
    structlog.configure(
        processors=log_processors,
        context_class=dict,
        logger_factory=structlog.stdlib.LoggerFactory(),
        wrapper_class=structlog.stdlib.BoundLogger,
        cache_logger_on_first_use=True,
    )
    logging.basicConfig(
        format="%(message)s",
        stream=sys.stdout,
        level=logging.DEBUG if verbose else logging.INFO,
    )


@click.command()
@click.option(
    '-c',
    '--config',
    'config_path',
    help='b9 config file.',
    type=click.Path(exists=True),
    required=True,
)
@click.option(
    '-v/--verbose', 'verbose', default=False, help='Enable verbose logging.'
)
@click.version_option(version=__version__)
def main(config_path: typing.AnyStr, verbose: bool = False) -> None:
    """Watch and alert on configurable telemetry events."""
    setup_logging(verbose=verbose)
    click_completion.init()
    logger.info('Loading configuration', path=config_path)
    with open(config_path) as f:
        config = Config.loads(f.read())
    logger.info(f'Watching {len(config.inputs)} input(s)')
    watcher = Watcher(config=config)
    for alert in watcher.run():
        logger.warning(
            'Alert triggered', uuid=alert.uid.hex, rule=alert.rule.name
        )
        logger.debug(
            'Alert triggered', alert=json.dumps(alert.asdict(), indent=2)
        )


if __name__ == '__main__':
    main()
